package Clases;

/**
 * Created by Ventas on 5/07/2017.
 */
public class Especialista {
    private String nombre;
    private int annosExp;

    public Especialista() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getAnnosExp() {
        return annosExp;
    }

    public void setAnnosExp(int annosExp) {
        this.annosExp = annosExp;
    }
}
